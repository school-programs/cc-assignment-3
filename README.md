﻿<h1>Assignment 3</h1>
<h4>UNK Cloud Computing (CSIT-499-02)</h4>

<p>Work done by <b>Brett Garver</b></p>

<p><b>Things to Note</b></p>

<ul>
	<li>Change your configurations in the "Form1.cs" file first before running</li>
	<li><ul>
		<li>updateThreshold - this is the amount of time until another update happens</li>
		<li>syncDir - this should be the full path to the folder you want to synchronize</li>
		<li>bucketName - This will be the name of the bucket you want to synchronize on AWS</li>
		<li>bucketRegion - This is the AWS region your bucket resides in</li>
		<li>accessKey - This is provided to you by AWS in the IAM services under the user you wish to use</li>
		<li>secretKey - This is provided to you by AWS in the IAM services under the user you wish to use</li>
	</ul></li>
	<li>The first update process that happens on launch of program will synchronize both directories to match by downloading or uploading missing files</li>
</ul>