﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace CC_Assignment_S3_Sync
{
    public class clsAwsS3
    {
        string accessKey { get; set; }
        string secretKey { get; set; }
        string bucket { get; set; }
        RegionEndpoint region { get; set; }
        IAmazonS3 client;

        public clsAwsS3(string strBucket, string strAccessKey, string strSecretKey, RegionEndpoint region)
        {
            this.bucket = strBucket;
            this.accessKey = strAccessKey;
            this.secretKey = strSecretKey;
            this.region = region;
            login();
        }

        private void login()
        {
            client = new AmazonS3Client(accessKey, secretKey, region);
        }

        public List<S3Object> getBucketFileInfo(string strPrefix = "")
        {
            ListObjectsV2Request listRequest;
            List<S3Object> s3Objects = new List<S3Object>();

            if (strPrefix == "")
                listRequest = new ListObjectsV2Request
                {
                    BucketName = bucket
                };
            else
                listRequest = new ListObjectsV2Request
                {
                    BucketName = bucket,
                    Prefix = strPrefix
                };

            ListObjectsV2Response listResponse;

            do
            {
                listResponse = client.ListObjectsV2(listRequest);

                foreach (S3Object awsObject in listResponse.S3Objects)
                {
                    string test = awsObject.Key.Substring(awsObject.Key.Length - 1);
                    if (awsObject.Key.Substring(awsObject.Key.Length - 1) != "/")
                    {
                        s3Objects.Add(awsObject);
                    }
                }

                listRequest.ContinuationToken = listResponse.NextContinuationToken;
            } while (listResponse.IsTruncated);

            List<S3FileInfo> files = new List<S3FileInfo>();

            foreach (S3Object s3Object in s3Objects)
            {
                files.Add(new S3FileInfo(client, bucket, s3Object.Key));
            }

            return s3Objects;
        }

        public void downloadBucketFiles(string strItemKey, string strDestination)
        {
            GetObjectRequest request = new GetObjectRequest
            {
                BucketName = bucket,
                Key = strItemKey
            };

            using (GetObjectResponse response = client.GetObject(request))
            {
                bool downloaded = false;
                while (!downloaded)
                {
                    try
                    {
                        response.WriteResponseStreamToFile(strDestination, true);
                        downloaded = true;
                    }
                    catch (DirectoryNotFoundException e)
                    {
                        new DirectoryInfo(strDestination).Create();
                    }
                    catch (IOException) 
                    { 
                        //do nothing
                    }
                }
            }
        }

        public void deleteBucketFile(string strItemKey)
        {
            DeleteObjectRequest deleteObject = new DeleteObjectRequest
            {
                BucketName = bucket,
                Key = strItemKey
            };

            DeleteObjectResponse deleteResponse = client.DeleteObject(deleteObject);
        }

        public async void uploadFiletoBucket(string filePath, string bucketName, string key)
        {
            try
            {
                var fileTransferUtility = new TransferUtility(client);

                await fileTransferUtility.UploadAsync(filePath, bucketName, key);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }
        }
    }
}
