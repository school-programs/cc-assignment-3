﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace CC_Assignment_S3_Sync
{
    public partial class Form1 : Form
    {
        //aws class variables
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast2;
        private const string bucketName = "ccassignment3";
        private const string accessKey = "AKIAWBMQKOG6S2UVCUFT";
        private const string secretKey = "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";
        private static clsAwsS3 awsIntegration = new clsAwsS3(bucketName, accessKey, secretKey, bucketRegion);

        //Other Class Variables
        private string syncDir = @"C:\Users\bgarv\OneDrive\Shared School\Brett\UNK\2019 - Fall\Cloud Computing\Assignment 3\";
        private int updateThreshold = 10;
        private List<SyncFile> currentFileList = new List<SyncFile>();
        private List<FileInfo> scannedLocalList = new List<FileInfo>();
        private List<S3Object> scannedAWSList = new List<S3Object>();
        private bool firstRun = true;

        public Form1()
        {
            InitializeComponent();
            syncRunner();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (updateThreshold == 0)
            {
                syncRunner();  
            }
            else
            {
                Text = "Next Update in " + updateThreshold;
                updateThreshold--;
            }

        }

        private void syncRunner()
        {
            Text = "Updating. Please Wait...";
            timer1.Stop();
            updateThreshold = 10;
            buildFileList();
            synchronize();
            if (!firstRun)
                checkDeletions();
            scannedAWSList.Clear();
            scannedLocalList.Clear();

            foreach (SyncFile syncFile in currentFileList)
            {
                syncFile.resetChecks();
            }
            timer1.Start();
            if (firstRun)
                firstRun = false;
        }

        private void buildFileList()
        {
            localFileScan(syncDir);
            scannedAWSList = awsIntegration.getBucketFileInfo();

            foreach (FileInfo fileInfo in scannedLocalList)
            {
                SyncFile syncFile = currentFileList.Find(temp => temp.GetLocalCopyFullPath() == fileInfo.FullName);

                if (syncFile == null) //file might be new, but check to see if aws copy exists
                {
                    Debug.WriteLine(fileInfo.FullName);
                    syncFile = currentFileList.Find(temp => fileInfo.FullName.EndsWith(temp.GetAwsCopyKey().Replace('/', '\\')) && temp.GetAwsCopyKey() != "");

                    if (syncFile == null) //aws copy doesn't exists so this is an entirely new file
                    {
                        syncFile = new SyncFile();
                        syncFile.AddLocalFile(fileInfo);
                        currentFileList.Add(syncFile);
                    }
                    else //local file exists so reference this copy
                    {
                        syncFile.AddLocalFile(fileInfo);
                    }
                }
                else //file exists in list already so check modified dates
                {
                    if (fileInfo.LastWriteTime > syncFile.GetLocalLastMod())
                        syncFile.AddLocalFile(fileInfo);
                }

                syncFile.localExistanceCheck = true;
            }

            foreach (S3Object s3Object in scannedAWSList)
            {
                SyncFile syncFile = currentFileList.Find(temp => temp.GetAwsCopyKey() == s3Object.Key);

                if (syncFile == null) //file might be new, but check to see if local copy exists
                {
                    syncFile = currentFileList.Find(temp => temp.GetLocalCopyAwsFormattedKey() == s3Object.Key);

                    if (syncFile == null) //local file doesn't exist so this is an entirely new file
                    {
                        syncFile = new SyncFile();
                        syncFile.AddAWSFile(s3Object);
                        currentFileList.Add(syncFile);
                    }
                    else //local file exists so reference this copy
                    {
                        syncFile.AddAWSFile(s3Object);
                    }
                } 
                else //file exists in list already so check modified dates
                {
                    if (s3Object.LastModified > syncFile.GetAWSLastMod())
                        syncFile.AddAWSFile(s3Object);
                }

                syncFile.awsExistanceCheck = true;
            }
        }

        private void localFileScan(string directory)
        {
            string[] files = System.IO.Directory.GetFiles(directory);
            string[] dirs = Directory.GetDirectories(directory);

            foreach (string dir in dirs)
            {
                localFileScan(dir);
            }

            foreach (string curFile in files)
            {
                System.IO.FileInfo fi = null;
                try
                {
                    fi = new System.IO.FileInfo(curFile);
                    scannedLocalList.Add(fi);
                }
                catch (System.IO.FileNotFoundException fnf)
                {
                    //do nothing
                }
            }
        }

        private void synchronize()
        {
            foreach (SyncFile file in currentFileList)
            {
                switch (file.getUpdateStatus())
                {
                    case 1:
                        lstLog.Items.Add("Downloading " + file.GetAwsCopyKey() + " from AWS");
                        awsIntegration.downloadBucketFiles(file.GetAwsCopyKey(), syncDir + file.GetAwsCopyKey());
                        break;
                    case 2:
                        lstLog.Items.Add("Uploading " + file.GetLocalCopyAwsFormattedKey() + " to AWS");
                        awsIntegration.uploadFiletoBucket(file.GetLocalCopyFullPath(), bucketName, file.GetLocalCopyAwsFormattedKey());
                        break;
                }
            }
        }

        private void checkDeletions()
        {
            List<SyncFile> deletions = new List<SyncFile>();

            foreach (SyncFile syncFile in currentFileList)
            {
                //if local exists but wasn't checked that means local file was deleted so delete the aws copy
                if (!syncFile.localIsNull() && !syncFile.localExistanceCheck)
                {
                    lstLog.Items.Add("Deleting AWS file " + syncFile.GetAwsCopyKey());
                    awsIntegration.deleteBucketFile(syncFile.GetAwsCopyKey());
                    deletions.Add(syncFile);
                    break;
                }

                //if aws file exists but wasn't checked that means the aws file was deleted so delete the local copy
                if (!syncFile.awsIsNull() && !syncFile.awsExistanceCheck)
                {
                    lstLog.Items.Add("Deleting local file " + syncFile.GetLocalCopyAwsFormattedKey());
                    File.Delete(syncFile.GetLocalCopyFullPath());
                    deletions.Add(syncFile);
                    break;
                }
            }

            foreach (SyncFile syncFile in deletions)
            {
                currentFileList.Remove(syncFile);
            }

            deletions.Clear();
        }
    }
}
