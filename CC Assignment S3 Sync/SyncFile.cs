﻿using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC_Assignment_S3_Sync
{
    class SyncFile
    {
        private S3Object awsCopy;

        private FileInfo localCopy;

        private string awsFormattedLocalPath;
        public bool awsExistanceCheck { get; set; }
        public bool localExistanceCheck { get; set; }
        public bool awsUpdated { get; set; }
        public bool localUpdated { get; set; }
        private int updateStatus;

        public SyncFile()
        {
            awsExistanceCheck = false;
            localExistanceCheck = false;
            awsUpdated = false;
            localUpdated = false;
        }

        public void AddAWSFile(S3Object s3Object)
        {
            awsCopy = s3Object;
            if (!awsUpdated)
            {
                localUpdated = true;
                updateStatus = 1; //file needs downloaded to local
            }
            else
            {
                awsUpdated = false;
            }
        }

        public void AddLocalFile (FileInfo fileInfo)
        {
            localCopy = fileInfo;
            string tempPath;
            try
            {
                tempPath = fileInfo.DirectoryName.Substring(89).Replace('\\', '/') + "/";
            }
            catch (ArgumentOutOfRangeException)
            {
                tempPath = "";
            }
            tempPath += fileInfo.Name;
            awsFormattedLocalPath = tempPath;

            if (!localUpdated)
            {
                awsUpdated = true;
                updateStatus = 2; //file needs uploaded to aws
            }
            else
            {
                localUpdated = false;
            }
        }

        public int GetDeleteValue()
        {
            int status = 0;
            if (awsCopy == null)
            {
                status = 1;
            }
            if (localCopy == null)
            {
                status = 2;
            }
            if (localCopy == null && awsCopy == null)
            {
                status = 3;
            }
            return status;
        }

        public string GetLocalCopyAwsFormattedKey()
        {
            try
            {
                return awsFormattedLocalPath;
            }
            catch (NullReferenceException)
            {
                return "";
            }
        }

        public string GetLocalCopyFullPath()
        {
            try
            {
                return localCopy.FullName;
            }
            catch (NullReferenceException)
            {
                return "";
            }
        }

        public string GetAwsCopyKey()
        {
            try
            {
                Debug.WriteLine(awsCopy.Key + "\n");
                return awsCopy.Key;
            }
            catch (NullReferenceException)
            {
                Debug.WriteLine("\n");
                return "";
            }
        }

        public DateTime GetAWSLastMod()
        {
            try
            {
                return awsCopy.LastModified;
            }
            catch (NullReferenceException)
            {
                return DateTime.MinValue;
            }
            
        }

        public DateTime GetLocalLastMod()
        {
            try
            {
                return localCopy.LastWriteTime;
            }
            catch (NullReferenceException)
            {
                return DateTime.MinValue;
            }
        }

        public int getUpdateStatus()
        {
            if (awsUpdated || localUpdated)
            {
                return updateStatus;
            }
            else
            {
                return 0;
            }
        }

        public bool getAwsExistanceChecked()
        {
            return awsExistanceCheck;
        }

        public bool getLocalExistanceChecked()
        {
            return localExistanceCheck;
        }

        public bool awsIsNull()
        {
            return awsCopy == null;
        }

        public bool localIsNull()
        {
            return localCopy == null;
        }

        public void resetUpdateStatus()
        {
            updateStatus = 0;
        }

        public void resetChecks()
        {
            awsExistanceCheck = false;
            localExistanceCheck = false;
            updateStatus = 0;
        }
    }
}
